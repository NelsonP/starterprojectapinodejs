const express = require('express');
const {signup, signin} = require('../services/authenticationService');
const requireAuth = require('../services/auth');
const router = express.Router();

router.get('/', requireAuth)

router.post('/signup', signup);

router.post('/signin', signin);

module.exports = router;