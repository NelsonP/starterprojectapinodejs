const express = require('express');
const {getUser} = require('../services/userService');
const requireAuth = require('../services/auth');
const router = express.Router();

router.get('/', requireAuth, getUser);

module.exports = router;