# StarterProjectAPINodeJS

This project is an initialization of a Node JS API with user creation.

## Dependencies 

* [bcryptjs](https://www.npmjs.com/package/bcryptjs): 

	A library for hashing and securely verifying passwords.

* [chai](https://www.npmjs.com/package/chai): 

	An assertion library for Node.js and the browser.

* [dotenv](https://www.npmjs.com/package/dotenv): 

	A library to load environment variables from a `.env` file into `process.env`.

* [express](https://github.com/expressjs/express): 

	A minimalist framework for Node.js, used for building web applications and APIs.

* [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken): 

	A library for generating and verifying JSON Web Tokens (JWT).

* [pg](https://github.com/brianc/node-postgres): 

	A non-blocking PostgreSQL client for Node.js.

* [pg-hstore](https://github.com/scarney81/pg-hstore): 

	A serialization and deserialization module for PostgreSQL's hstore data type.

* [sequelize](https://github.com/sequelize/sequelize): 

	An ORM (Object-Relational Mapper) for Node.js.

## Development Dependencies

* [kill-port](https://www.npmjs.com/package/kill-port): 

	A tool to free up TCP ports.

* [mocha](https://www.npmjs.com/package/mocha): 

	A testing framework for Node.js.

* [nodemon](https://www.npmjs.com/package/nodemon): 

	A utility that monitors all changes in source code and restarts the server.

* [sinon](https://www.npmjs.com/package/sinon): 

	A testing library that provides spies, stubs, and mocks for JavaScript.

* [proxyquire](https://www.npmjs.com/package/proxyquire): 

	A tool to replace required modules with other modules, for more isolated testing.

## Initialization

Follow these steps to initialize the project:

1. **Define the environment variables:**

	Create a .env file in which the environment variables are specified as follows:

	```javascript
	LISTENING_PORT=...

	DB_NAME=...
	DB_USER_NAME=...
	DB_USER_PASSWORD=...
	DB_HOST=...
	DB_PORT=...
	```
	
	Replacing the following variables:
	
	* LISTENING_PORT: Server listening port (ex: 3000)

	* DB_NAME: Name of the database located in the "Dtabases" section of the database.

	* DB\_USER\_NAME: Username located in the "Users" section of the database.

	* DB\_USER\_PASSWORD: Password associated with the username.

	* DB_HOST: Address of the database (ex: "postgresql-521c417c-o61054b3c.database.cloud.ovh.net") located in the "General Information" section of the database.

	* DB_PORT: Database communication port located in the "General Information" section of the database.

2. **Download the database CA certificate:**

	On OVH in the "General Information" section of the database, download the ca.pem certificate and place it at the root of the project.

3. **Add the server IP address:**

	On OVH in the "Authorized IPs" section of the database, add the IP address of the server running this API.

4. **Install the dependencies:**

	Run the following command to install the project dependencies:

	`npm install`

5. **Compile the project:**

	Run the following command to build the Docker container of the project:

	`sudo docker build -t starter-project-api-node-js .`

6. **Launch the API:**

	Run the following command to start the Docker container, which will automatically launch the API:

	`sudo docker run -p 3000:3000 starter-project-api-node-js`

	This will put the API into operation, listening on port 3000.

## License

This project is licensed under the MIT License.

We hope you'll enjoy using this project as a starting point for your next SwiftUI application!

## Contact

If you have any questions, feel free to open an issue or submit a pull request. Happy coding!
