require('dotenv').config()

const environmentVariables = {
    app: {
      listeningPort: process.env.LISTENING_PORT,
      secretKey: process.env.SECRET_KEY,
    },
    database: {
        name: process.env.DB_NAME,
        userName: process.env.DB_USER_NAME,
        userPassword: process.env.DB_USER_PASSWORD,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
    }
};
  
module.exports = environmentVariables;