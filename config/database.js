const { Sequelize } = require('sequelize');
const fs = require('fs');
const environmentVariables = require('./environmentVariables');

let caCertificate;
if (process.env.NODE_ENV !== 'test') {
  caCertificate = fs.readFileSync('ca.pem').toString();
}

const sequelize = new Sequelize(environmentVariables.database.name, environmentVariables.database.userName, environmentVariables.database.userPassword, {
    host: environmentVariables.database.host,
    port: environmentVariables.database.port,
    dialect: 'postgres',
    logging: console.log,
    dialectOptions: {
        ssl: {
            require: true,
            rejectUnauthorized: true, 
            ca: caCertificate
        }
    }
});

module.exports = sequelize;