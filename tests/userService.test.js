const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;
const proxyquire = require('proxyquire');

module.exports = function () {
    describe('user service', function () {
        // set timeout to 5000 ms
        this.timeout(5000);

        beforeEach(function () {
            // set timeout to 5000 ms
            this.timeout(5000);

            // Create mocks for dependencies
            userModel = {
                findByPk: sinon.stub(),
                create: sinon.stub(),
                findOne: sinon.stub()
            };

            jwt = {
                sign: sinon.stub()
            };

            // Create mocks for dependencies
            userController = proxyquire('../services/userService', {
                '../models/user': userModel,
                'jsonwebtoken': jwt
            });
        });

        afterEach(function () {
            if (userModel.findByPk.restore) {
                userModel.findByPk.restore();
            }
        });

        it('should get a user', async function () {
            // set timeout to 5000 ms
            this.timeout(5000);

            const req = {
                headers: { authorization: 'fakeToken' },
                decoded: { userId: '123' }
            };
            const res = {
                send: sinon.stub()
            };

            // Set mocks
            userModel.findByPk.returns(Promise.resolve({ id: '123' }));
            jwt.sign.returns('fakeToken');

            // Call function to test
            await userController.getUser(req, res);

            // Check expectations
            expect(res.send.calledWith({ id: '123' })).to.be.true;
        });   
    });
};