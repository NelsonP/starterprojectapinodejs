const mocha = require('mocha');

// Import your test files
const authenticationServiceTests = require('./authenticationService.test.js');
const userServiceTests = require('./userService.test.js');

// Describe the suite of all tests
describe('All tests', function() {
  authenticationServiceTests();
  userServiceTests();
});