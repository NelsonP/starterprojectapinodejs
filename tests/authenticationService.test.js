const sinon = require('sinon');
const proxyquire = require('proxyquire');
const chai = require('chai');
const expect = chai.expect;

// Wrap your tests in a function
module.exports = function() {
  describe('authentication service', function () {
    // set timeout to 5000 ms
    this.timeout(5000); 
  
    let authController, userModel, jwt;
  
    beforeEach(function () {
      // set timeout to 5000 ms
      this.timeout(5000); 
  
      try {
  
        // Create mocks for dependencies
        userModel = {
          create: sinon.stub(),
          findOne: sinon.stub()
        };
  
        jwt = {
          sign: sinon.stub()
        };
  
        // Create mocks for dependencies
        authController = proxyquire('../services/authenticationService', {
          '../models/user': userModel,
          'jsonwebtoken': jwt
        });
      } catch (error) {
        console.error("Erreur lors de l'exécution de beforeEach:", error);
      }
    });
  
    it('should sign up a user', async function () {
      // set timeout to 5000 ms
      this.timeout(5000);
  
      const req = {
        body: { loginId: 'test', password: 'test', isAppleAccount: 'false' }
      };
      const res = {
        send: sinon.stub()
      };
  
      // Set mocks
      userModel.create.returns(Promise.resolve({ id: '123' }));
      jwt.sign.returns('token');
  
      // Call function to test
      await authController.signup(req, res);
  
      // Check expectations
      expect(userModel.create.calledWith(req.body)).to.be.true;
      expect(jwt.sign.calledWith({ userId: '123' })).to.be.true;
      expect(res.send.calledWith({ token: 'token' })).to.be.true;
    });
  
    it('should sign in a user', async function () {
      // set timeout to 5000 ms
      this.timeout(5000); 
    
      const req = {
        body: { loginId: 'test', password: 'test' }
      };
      const res = {
        send: sinon.stub(),
        status: sinon.stub().returnsThis()
      };
  
      // Mock user object with comparePassword method
      const mockUser = {
        id: '123',
        comparePassword: sinon.stub()
      };
  
      // Configure the mocks to return specific values
      userModel.findOne.returns(Promise.resolve(mockUser));
      mockUser.comparePassword.returns(Promise.resolve(true));
      jwt.sign.returns('token');
  
      // Call the function to test
      await authController.signin(req, res);
  
      // Check that the mocks were called as expected
      expect(userModel.findOne.calledWith({ where: { loginId: req.body.loginId } })).to.be.true;
      expect(mockUser.comparePassword.calledWith(req.body.password)).to.be.true;
      expect(jwt.sign.calledWith({ userId: mockUser.id })).to.be.true;
      expect(res.send.calledWith({ token: 'token' })).to.be.true;
    });
  });
};