const sendAuthError = (res) => {
  return res.status(422).send({ error: 'Invalid username or password' });
};

const sendUserAlreadyCreatedError = (res) => {
  return res.status(409).send({ error: 'This user id is already used.' });
};

const sendInternalServerError = (res) => {
  return res.status(500).send({ error: 'An error occurred while creating the user.' });
};

const sendAuthentFailedServerError = (res) => {
  return res.status(498).send({ error: 'Failed to authenticate token.' });
};

const sendNoTokentError = (res) => {
  return res.status(498).send({ error: 'No token provided.' });
};

const sendUserNotFoundError = (res) => {
  return res.status(404).send({ error: 'User not found' });
};

module.exports = {
  sendAuthError,
  sendUserAlreadyCreatedError,
  sendInternalServerError,
  sendAuthentFailedServerError,
  sendNoTokentError,
  sendUserNotFoundError
};