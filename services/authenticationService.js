const jwt = require('jsonwebtoken');
const User = require('../models/user');
const environmentVariables = require('../config/environmentVariables');
const {sendAuthError, sendUserAlreadyCreatedError, sendInternalServerError} = require('./errors');

const generateToken = (userId) => {
  return jwt.sign({ userId }, environmentVariables.app.secretKey);
};

const signup = async (req, res) => {
  const { loginId, password, isAppleAccount } = req.body;
  
  try {
    const user = await User.create({ loginId, password, isAppleAccount });

    const token = generateToken(user.id);
    res.send({ token });
  } catch (error) {
    if (error.name === 'SequelizeUniqueConstraintError') {
      return sendUserAlreadyCreatedError(res);
    }
    
    console.error(error);
    return sendInternalServerError(res);
  }
};

const signin = async (req, res) => {
  const { loginId, password } = req.body;
  const user = await User.findOne({ where: { loginId } });
  if (!user) {
    return sendAuthError(res);
  }

  try {
    if (await user.comparePassword(password)) {
      const token = generateToken(user.id);
      return res.send({ token });
    } else {
      return sendAuthError(res);
    }
  } catch (err) {
    return sendAuthError(res);
  }
};

module.exports = {
  signup,
  signin
};