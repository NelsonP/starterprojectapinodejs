const User = require('../models/user');
const { sendUserNotFoundError, sendInternalServerError } = require('./errors');

const getUser = async (req, res) => {
  const userId = req.decoded.userId;

  try {
    const user = await User.findByPk(userId);
    if (!user) {
      return sendUserNotFoundError(res);
    }
    delete user.password;
    res.send(user);
  } catch (error) {
    console.error(error);
    return sendInternalServerError(res);
  }
};

module.exports = {
  getUser
};