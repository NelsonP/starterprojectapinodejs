const jwt = require('jsonwebtoken');
const environmentVariables = require('../config/environmentVariables');
const {sendAuthentFailedServerError, sendNoTokentError} = require('./errors');

const requireAuth = (req, res, next) => {
  const token = req.headers.authorization;

  if (!token) {
    return sendNoTokentError(res);
  }

  jwt.verify(token, environmentVariables.app.secretKey, (err, decoded) => {
    if (err) {
      return sendAuthentFailedServerError(res);
    } else {
      // if everything is good, save the decoded token for request for use in other routes
      req.decoded = decoded;
      next();
    }
  });
};

module.exports = requireAuth;
