require('dotenv').config();
const express = require('express');
const sequelize = require('./config/database');
const authenticationRoutes = require('./api/authenticationRoutes');
const userRoutes = require('./api/userRoutes');
const app = express();
const environmentVariables = require('./config/environmentVariables');
const PORT = environmentVariables.app.listeningPort || 3000;  // Utilisez la variable d'environnement PORT, ou 3000 si PORT n'est pas défini.

app.use(express.json());
app.use('/authentication', authenticationRoutes);
app.use('/user', userRoutes);

sequelize.sync().then(() => {
  app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
  });
}).catch(console.error);

module.exports = app;